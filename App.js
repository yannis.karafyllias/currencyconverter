import { MenuProvider } from 'react-native-popup-menu';
import MainNavigation from './navigation/MainNavigation';
import { Provider } from 'react-redux';
import { store } from './store/store';

export default App = () => {
  return (
    <Provider store={store}>
        <MenuProvider>
          <MainNavigation />
        </MenuProvider>
    </Provider>
  );
}