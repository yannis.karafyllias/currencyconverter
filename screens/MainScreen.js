import React, { useState, useEffect } from 'react';
import { View, Keyboard, TouchableWithoutFeedback, Platform } from 'react-native';
import axios from 'axios';
import Styles from '../utils/styles';
import { Text, Input } from '@rneui/base';
import CurrencyDropdown from '../components/CurrencyDropdown';
import * as Config from '../utils/config';

const MainScreen = () => {
  const [amount, setAmount] = useState('');
  const [targetCurrency, setTargetCurrency] = useState('USD');
  const [convertedAmount, setConvertedAmount] = useState('');
  const [error, setError] = useState('')

  useEffect(() => {
    if (amount) {
      // Clear previous converted amounts
      setConvertedAmount('')

      // use exchangerate-api in order to produce the new converted amount, passing the API_KEY, the target Currency and the amount
      axios.get(`https://v6.exchangerate-api.com/v6/${Config.EXCHANGE_RATE_API_KEY}/pair/EUR/${targetCurrency}/${amount}`)
        .then(response => {
          setError('')
          setConvertedAmount(response.data.conversion_result.toFixed(2)); // Rounds the converted ammount to two decimals
        })
        .catch(error => {
          setError(error.toString())
        });
    }
  }, [amount, targetCurrency]); // run the effect when the amount or the targetCurrency changes

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={Styles.container}>
        <Text style={Styles.text}>Amount (EUR)</Text>
        <Input 
          value={amount} 
          onChangeText={setAmount} 
          keyboardType="numeric" 
          inputContainerStyle={[Styles.input, {paddingLeft: 10}]}
          inputStyle={Styles.inputText}
          containerStyle={{paddingHorizontal: 0}}
        />
        <Text style={Styles.text}>Please select a target currency</Text>
        <CurrencyDropdown 
          selectedCurrency={targetCurrency} 
          onSelectCurrency={setTargetCurrency} 
        />
        <View style={{marginTop: 40, zIndex: -1}}>
          <Text style={[Styles.text, {textAlign: 'center', textDecorationLine: 'underline'}]}>Converted Amount</Text> 
          <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'baseline' }}>
            <Text h4 style={Styles.text}>{convertedAmount}</Text>
            {convertedAmount && <Text style={Styles.text}> {targetCurrency}</Text>}
          </View>
        </View>
        {error && <Text style={Styles.error}>{error}</Text>}
        </View>
    </TouchableWithoutFeedback>
  );
};

export default MainScreen;