import React, { useState } from 'react';
import { TouchableWithoutFeedback, Keyboard, KeyboardAvoidingView, Platform} from 'react-native';
import { Button, Input, Text } from '@rneui/base';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Styles from '../utils/styles';
import { useDispatch, useSelector } from 'react-redux';
import Colors from '../utils/colors';
import { setUser, setError, clearError } from '../store/slices/authSlice';
import firebase from "../utils/firebase";


const LoginScreen = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVisible, setPasswordVisible] = useState(false);
  const error = useSelector((state) => state.auth.error);
  const dispatch = useDispatch();

  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  //Handles sign  in
  const handleSubmit = async () => {
    if (email === '' | password === '' ) {
      // If email or password is missing show an error
      dispatch(setError('Please enter valid email and password'))
    }
    else {
      try {
        // Clear previous errors
          dispatch(clearError());
          // Firebase authentication
          const { user } = await firebase.auth().signInWithEmailAndPassword(email, password);
          // Dispatch an action in order to save the logged in user in the central app state
          dispatch(setUser(user));
      } catch(err) {
          // Handle authentication errors
          dispatch(setError(err.toString()));
      }
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}  style={Styles.container}>
        <Text style={Styles.text}>Enter your account credentials</Text>
        <Input 
            placeholder='Email' 
            onChangeText={value => setEmail(value.trim())}
            errorMessage={''}
            leftIcon={<Icon name="email" size={30} color={Colors.primary}/>}
            disabledInputStyle={{ background: "#ddd" }}
            inputContainerStyle={Styles.input}
            leftIconContainerStyle={Styles.icon}
            containerStyle={{paddingHorizontal: 0}}
            inputStyle={Styles.inputText}
        />
        <Input 
            placeholder='Password' 
            onChangeText={value => setPassword(value.trim())}
            errorMessage={''}
            leftIcon={<Icon name="lock" size={30} color={Colors.primary} />}
            rightIcon={
              <Icon 
                name={!passwordVisible ? 'eye-off' : 'eye'} 
                size={20} 
                onPress={togglePasswordVisibility}
                color={Colors.primary}
              />
            }
            rightIconContainerStyle={Styles.icon}
            leftIconContainerStyle={Styles.icon}
            disabledInputStyle={{ background: "#ddd" }}
            secureTextEntry={!passwordVisible}
            inputContainerStyle={Styles.input}
            containerStyle={{paddingHorizontal: 0}}
            inputStyle={Styles.inputText}
        />
        <Button 
          title='Login' 
          buttonStyle={Styles.button} 
          titleStyle={Styles.buttonText}
          onPress={handleSubmit}
          color={Colors.primary}
        />
        {error && <Text style={Styles.error}>{error}</Text>}
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  )
};

export default LoginScreen;