import { createSlice } from "@reduxjs/toolkit"

const authSlice = createSlice({
  name: "auth",
  initialState: {
    user: null,
    error: null
  },
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload; // with redux-toolkit we can mutate the state
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    clearError: (state) => {
      state.error = null;
    },
    clearUser: (state) => {
      state.user = null;
    }
  }
});

export const { setUser, setError, clearError, clearUser } = authSlice.actions;
export default authSlice.reducer;