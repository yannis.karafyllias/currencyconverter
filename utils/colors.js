export default {
  background: '#222831',
  primary: '#00B5BC',
  secondary: '#393E46',
  text: '#ffffff',
  textInput: '#eeeeee'
};