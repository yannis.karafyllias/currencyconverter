import { StyleSheet ,Dimensions} from 'react-native';
import Colors from './colors';
import * as Config from './config';

const width = Dimensions.get('window').width - 2*Config.STANDARD_MARGIN; //full width

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.background,
    paddingHorizontal: Config.STANDARD_MARGIN,
  },
  button: {
    width: width,
    backgroundColor: Colors.primary,
    marginTop: Config.STANDARD_MARGIN,
    borderRadius: 10,
    paddingVertical: 15
  },
  buttonText: {
    color: '#000000'
  },
  input: {
    backgroundColor: Colors.background,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: Colors.secondary,
    paddingVertical: 5,
    width: width,
  },
  inputText: {
    color: Colors.textInput
  },
  icon: {
    paddingRight: 15,
    paddingLeft: 15
  },
  text: {
    color: '#ffffff',
    fontSize: 15,
    marginBottom: 20
  },
  error: {
    color: 'red',
    textAlign: 'center', 
    marginTop: 20 
  },
  dropDown: {
    borderColor: Colors.secondary,
    borderRadius: 10,
    borderWidth: 2,
  }
});