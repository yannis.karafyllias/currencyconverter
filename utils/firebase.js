import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const firebaseConfig = {
  apiKey: "AIzaSyAuV8cbZ-qlZPpW3Qekr_ktY9OKlo0gQBQ",
  authDomain: "currencyconverter-dc60d.firebaseapp.com",
  projectId: "currencyconverter-dc60d",
  storageBucket: "currencyconverter-dc60d.appspot.com",
  messagingSenderId: "534323861739",
  appId: "1:534323861739:web:b5ab5cc2bcdbcf455046ae",
  measurementId: "G-NZ29KXCFMW"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default firebase;