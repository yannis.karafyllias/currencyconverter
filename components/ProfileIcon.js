import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Menu,  MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import { getAuth, signOut } from 'firebase/auth';
import { clearUser } from '../store/slices/authSlice';
import { useDispatch } from 'react-redux';
import Colors from '../utils/colors';

const ProfileIcon = () => {

    const dispatch = useDispatch();

    const handleLogout = async () => {
      try {
          const auth = getAuth();
          await signOut(auth);
          dispatch(clearUser());
      } catch (error) {
          console.log(error);
      }
    };

  return (
    <TouchableOpacity onPress={() => setMenuVisible(true)}>
      <Menu style={{alignSelf: 'center'}}>
        <MenuTrigger>
          <Icon name='account' size={30} color={Colors.primary}/>
        </MenuTrigger>
        <MenuOptions customStyles={{optionsContainer: {marginTop: 30}}}>
          <MenuOption style={{ padding: 10 }} onSelect={handleLogout} text='Logout' />
        </MenuOptions>
      </Menu>
    </TouchableOpacity>
  );
};

export default ProfileIcon;