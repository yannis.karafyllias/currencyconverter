import React, { useEffect, useState } from 'react';
import { View, Platform } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import axios from 'axios';
import * as Config from '../utils/config';
import Styles from '../utils/styles';

const CurrencyDropdown = ({ selectedCurrency, onSelectCurrency }) => {
    const [currencies, setCurrencies] = useState([]);
    const [open, setOpen] = useState(false);

    useEffect(() => {
        // Fetch currency data from the API
        axios.get(`https://v6.exchangerate-api.com/v6/${Config.EXCHANGE_RATE_API_KEY}/latest/EUR`)
          .then((response) => {
            const data = response.data.conversion_rates;
            const currencyOptions = Object.keys(data).map((currency) => ({
              label: currency,
              value: currency,
            }));
            // Stote currency options in order to use them in the DropDown picker component below
            setCurrencies(currencyOptions);
          })
          .catch((error) => {
            console.error('Error fetching currency data:', error);
          });
      }, []); // Run the effect only after the component has been mounted
    
    return (
      <View styles={{display: 'flex', marginBottom: 20}}>
        <DropDownPicker
          open={open}
          value={selectedCurrency}
          items={currencies}
          setOpen={setOpen}
          setValue={onSelectCurrency}
          theme="DARK"
          style={Styles.dropDown}
          placeholder="Select a currency"
        />
      </View>
    );
};

export default CurrencyDropdown;