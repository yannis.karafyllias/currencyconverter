import React from 'react';
import { Image } from 'react-native';

const NavHeaderTitle = () => (
  <Image
    source={require('../assets/logo.png')}
    style={{ width: 168, height: 30, alignSelf: 'center'}}
    resizeMode="contain"
  />
);

export default NavHeaderTitle;
