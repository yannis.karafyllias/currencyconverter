# Main README file for mobile app development

Table of Contents:

- [Installation](#installation)
- [File Structure](#file-structure)
- [Screens & Navigation](#screens-&-navigation)
- [Rest API](#rest-api)
- [State management](#state-management)
- [Build App](#build-app)

## Installation

- Install Expo Go on a physical device.
- Install Node.js LTS release
- Install yarn: `npm install --global yarn`
- Download and install this repository

`git clone https://gitlab.com/yannis.karafyllias/currencyconverter.git`

- Run Yarn to install packages described in package.json

`yarn`

- Start and serve the app

`npx expo start -c`

Scan the barcode that will open the app through the Expo Go App

- In order to login use the demo credentials below

Email: demo@example.com
Password: demopass

## File Structure

In our implemention we use the file structure below:

- Screens folder for the screen components. 
- Components folder for varius components.
- Navigation folder for the navigation logic.
- Utils folder for helper functions and utilities. 
- Store folder for the app stage management files.

## Screens & Navigation

An App consists of several screens which are combined in a navigator. Navigators can then also be combined/nested.

Navigators can either be screens as *tabs* or *stacks* of screens.

For now we are using a set of 2 stack navigators. 

1. A stack for the authenticated users called `AuthenticatedStack`.
2. A stack for the anon users called `AnonStack`.

## Rest API

We use firebase to provide an API for authentication purposes.
A firebase project has been created for that purpose, and the email/password provider has been activated. 
Upon successfull authentication firebase responds with an accessToken and a refreshToken, amongst the rest of the user information.

We also use 'exchange-rate' api in order to fetch the currency options and then produce the converted value.

## State management

We use redux and specificaly `redux-toolkit` for the state management.
A reducer for the authentication state data (user data, error data) has been implemented.

## Build app

We use EAS in order to build the app for android and ios platforms.
In order to trigger new builds use the command `eas build`