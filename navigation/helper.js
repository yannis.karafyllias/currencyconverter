import Colors from "../utils/colors";
import NavHeaderTitle from "../components/NavHeaderTitle";

export const screenOptions = {
  headerStyle: {
    height: 100,
    backgroundColor: Colors.background,
    elevation: 0,
    shadowOpacity: 0,
    borderWidth: 0,
  },
  headerTitle: () => <NavHeaderTitle />,
  headerTitleAlign: 'center'
};
