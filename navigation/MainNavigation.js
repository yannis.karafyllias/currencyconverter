import { NavigationContainer } from '@react-navigation/native';
import AuthenticatedStack from './AuthenticatedStack';
import AnonStack from './AnonStack';
import { useSelector } from 'react-redux';

export default MainNavigation = () => {
  const user = useSelector(state => state.auth.user);
  return (
    <NavigationContainer>
      { user ? <AuthenticatedStack /> : <AnonStack /> }
    </NavigationContainer>
  );
}