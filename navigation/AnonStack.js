import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from '../screens/LoginScreen';
import { screenOptions } from './helper';

const Stack = createNativeStackNavigator();

export default AnonStack = () => {
  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen 
        name="LoginScreen" 
        component={LoginScreen} 
        options={{headerShadowVisible: false}}  // This does not work if placed on screenOptions
      />
    </Stack.Navigator>
  );
}
