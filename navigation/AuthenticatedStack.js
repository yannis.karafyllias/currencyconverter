import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainScreen from '../screens/MainScreen';
import ProfileIcon from '../components/ProfileIcon';
import { screenOptions } from './helper';

const Stack = createNativeStackNavigator();

export default AuthenticatedStack = () => {
    return (
      <Stack.Navigator screenOptions={screenOptions}>
        <Stack.Screen 
          name="MainScreen" 
          component={MainScreen} 
          options={({navigation}) => ({
            headerRight: () => <ProfileIcon navigation={navigation} />, 
            headerLeft: () => false,
            headerShadowVisible: false // This does not work if placed on screenOptions
          })} 
        />
      </Stack.Navigator>
    );
  }